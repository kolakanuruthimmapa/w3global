import { Component } from '@angular/core';
import {FilterPipe} from './pipes'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'app';
	names:any;
	menus : any;
	constructor(){
        this.names = ['Prashobh','Abraham','Anil','Sam','Natasha','Marry','Zian','karan'];

        this.menus = [
		    {id: 1, name: 'Dashboard', icon:'home'},
		    {id: 2, name: 'Candidate', icon:'users'},
		    {id: 3, name: 'Dies Candidate', icon:'users'},
		    {id: 4, name: 'Jobs', icon:'user-md'},
		    {id: 5, name: 'Accounts', icon:'clipboard-check'},
		    {id: 6, name: 'Applicants', icon:'check-square'},
		    {id: 7, name: 'All Leads', icon:'leaf'},
		    {id: 8, name: 'Vendors', icon:'venus'},
		    {id: 9, name: 'Notifications', icon:'bell'},
		];
  	}
}
