import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'FilterPipe',
})
export class FilterPipe implements PipeTransform {
    transform(value: any, input: string) {
        if (input) {
            input = input.toLowerCase();
            return value.filter(function(category){
              return category.name.toLowerCase().indexOf(input.toLowerCase()) > -1;
            })
        }
        return value;
    }
}